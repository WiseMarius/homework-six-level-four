In this exercise, for serialization, was used a boost library... so it might not work to you.
If you get this error: Missing library libboost_serialization-vc140-mt-1_63.lib please follow these steps:

1. Open the project
2. Right click on the project solution.
3. Click on property.
4. Click on c/c++.
5. Click on general.
6. Select additional include library.
7. Include the library destination. e.g. c:\boost_1_57_0.
8. Click on pre-compiler header.
9. Click on create/use pre-compiled header.
10. Select not using pre-compiled header.

Then go over to the link library were you experienced your problems.

1. Go to were the extracted file was c:\boost_1_57_0.
2. Click on booststrap.bat (don't bother to type on the command window just wait and don't close the window. After a while the booststrap will run and produce the same file, but now with two different names: b2, and bjam.
3. Click on b2 and wait it to run.
4. Click on bjam and wait it to run. Then a folder will be produce called stage. Then go back to step 4.
5. Click on linker.
6. Click on general.
7. Click on include additional library directory.
8. Select the part of the library e.g. c:\boost_1_57_0\stage\lib.

And you are good to go!