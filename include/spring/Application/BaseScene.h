#pragma once
#include <spring\Framework\IScene.h>

//#include <QtCore/QVariant>
//#include <QtWidgets/QAction>
//#include <QtWidgets/QApplication>
//#include <QtWidgets/QButtonGroup>
//#include <QtWidgets/QGridLayout>
//#include <QtWidgets/QHeaderView>
//#include <QtWidgets/QListView>
//#include <QtWidgets/QMainWindow>
//#include <QtWidgets/QMenuBar>
//#include <QtWidgets/QPushButton>
//#include <QtWidgets/QSpacerItem>
//#include <QtWidgets/QStatusBar>
//#include <QtWidgets/QToolBar>
//#include <QtWidgets/QWidget>
#include <qcustomplot.h>

namespace Spring
{
	class BaseScene : public IScene
	{
		Q_OBJECT
	public:

		explicit BaseScene(const std::string& ac_szSceneName);

		void createScene() override;

		void reloadTransientData() override;

		void release() override;

		void saveTransientData() override;

		~BaseScene();
		
	private:
		QWidget *centralWidget;
		QGridLayout *gridLayout_2;
		QGridLayout *gridLayout;
		QPushButton *stopButton;
		QPushButton *startButton;
		QCustomPlot *listView;
		QSpacerItem *horizontalSpacer;
		QMenuBar *menuBar;
		QToolBar *mainToolBar;
		QPushButton *backButton;
		QStatusBar *statusBar;

		void createGUI();
		private slots:

		void mf_BackButton();
		
	};

}
