#pragma once
#include <spring\Framework\IScene.h>
#include <qgridlayout.h>
#include <qspinbox.h>
#include <qpushbutton.h>
#include <qlabel>
#include <qlineedit.h>
#include <qobject.h>

#include <fstream>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

namespace Spring
{
	class InitialScene : public IScene
	{
		Q_OBJECT

	public:

		explicit InitialScene(const std::string& ac_szSceneName);

		void createScene() override;

		void reloadTransientData() override;

		void release() override;

		void saveTransientData() override;

		~InitialScene();

	private:
		void createGUI();

		QWidget *centralWidget;
		QGridLayout *gridLayout_2;
		QGridLayout *gridLayout;
		QSpinBox *sampleRateSpinBox;
		QLineEdit *applicationNameLineEdit;
		QLabel *applicationNameLabel;
		QSpinBox *refreshRatespinBox;
		QLabel *sampleRateLabel;
		QSpacerItem *topVerticalSpacer;
		QSpacerItem *bottomVerticalSpacer;
		QLabel *displayTimeLabel;
		QLabel *refreshRateLabel;
		QSpacerItem *leftHorizontalSpacer;
		QSpacerItem *rightHorizontalSpacer;
		QPushButton *okButton;
		QDoubleSpinBox *displayTimeDoubleSpinBox;
		QToolBar *mainToolBar;
		QStatusBar *statusBar;

		private slots:
		void mf_OkButton();

	};

	//We need a data structure in order to make serialization possible.
	//Each scene must have this kind of structure. It contains only the data that we want to save 
	struct TransientData
	{
		std::string keyAppName;
		std::string appName;

		std::string keySampleRate;
		unsigned int sampleRate;

		std::string keyDiplsayTime;
		double displayTime;

		std::string keyRefreshRate;
		unsigned int refreshRate;

		//We will use this to create an empty TransientData structure (used for deserialize)
		TransientData();
		//Used to construct the structure with certain values
		TransientData(std::string, std::string, std::string, unsigned int, std::string, double, std::string, unsigned int);

		// When the class Archive corresponds to an output archive, the
		// & operator is defined similar to <<. Likewise, when the class Archive
		// is a type of input archive the & operator is defined similar to >>.
		template<class Archieve>
		void serialize(Archieve &, const unsigned int);
	};
}
